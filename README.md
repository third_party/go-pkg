go-pkg
======

This repository is a parent-project for Third party Golang repositories used by
Fuchsia projects.  Third party repos are placed beneath this namespace at
//third_party/go-pkg/$repo_name.
